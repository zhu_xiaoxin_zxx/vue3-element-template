import axios from "axios";
import { ElMessage } from "element-plus";
// import router from "../router";
const service = axios.create({
  timeout: 15 * 1000,
  withCredentials: true,
});

service.interceptors.request.use(
  function (config) {
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

service.interceptors.response.use(
  function (response) {
    let res = response.data;
    // if (res.code !== 0) {
    //   if (res.code === -1) {
    //     if (router.currentRoute.path !== "/login") {
    //       localStorage.setItem("target_uri", router.currentRoute.fullPath);
    //       router.replace("/login");
    //     }
    //   } else {
    //     ElMessage({
    //       message: res.msg || res.error || "Error",
    //       type: "error",
    //       duration: 5 * 1000,
    //     });
    //   }
    //   return Promise.reject(res);
    // } else {
    return res;
    // }
  },
  function (error) {
    ElMessage({
      message: error.message,
      type: "error",
      duration: 5 * 1000,
    });
    return Promise.reject(error);
  }
);

export default service;
