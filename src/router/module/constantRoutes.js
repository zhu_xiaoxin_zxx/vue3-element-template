export const constantRoutes = [
  {
    path: "/home",
    name: "Home",
    component: () => import("@/views/home/index.vue"),
    meta: {
      title: "首页",
      icon: "HomeFilled",
    },
  },
  {
    path: "/table",
    name: "Table",
    component: () => import("@/views/table/index.vue"),
    meta: {
      title: "表格",
      icon: "Document",
    },
  },
];
