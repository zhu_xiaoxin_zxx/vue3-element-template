import { createApp } from "vue";
import { createPinia } from "pinia";
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import App from "./App.vue";
import router from "./router";
import * as ElementPlusIconsVue from "@element-plus/icons-vue";
import "./styles/global.scss";
import http from "./server";
import apis from "./api";

const app = createApp(App);

//添加原型链
// app.config.globalProperties.$http = http;

//依赖注入
app.provide("$http", http);
app.provide("$apis", apis);

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component);
}
app.use(createPinia());
app.use(router);
app.use(ElementPlus);

app.mount("#app");
