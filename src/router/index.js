import { createRouter, createWebHashHistory } from "vue-router";
import Layout from "../layout/index.vue";
import { constantRoutes } from "./module/constantRoutes";

const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      path: "/",
      name: "Layout",
      component: Layout,
      redirect: "/home",
      children: [...constantRoutes],
    },
  ],
});

export default router;
